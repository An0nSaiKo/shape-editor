# Shape Editor

A Shape editor for CS150b, created by 2019-2020 class of CSD - University of Crete.

# Recommendations

To make your life easier install git.

Go to https://git-scm.com/download and follow the instractions for your OS.

For those who prefer using terminal over browser, good news [here](https://git-scm.com/downloads/guis/).


# Basics

**#Get the code**

To get started, make a local copy of the (git) repository:

`git clone https://gitlab.com/csd.uoc/2019_150b/shape-editor.git`

a new folder named 'shape-editor' should appear inside the directory you executed the above command. 

**#Setup current directory configurations**

Set username and email address to current project

Add your gitlab username 

` git config user.name "John Doe" `

confirm username 

`git config user.name`

Add your email address

` git config user.email johndoe@example.com `

confirm email 

`git config user.email`

**#Update it**

To update your local copy to the recent state of the repository:

`git pull`

[comprehensive git documentation](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)

**#Change Branch**

Whenever you clone a repository, usually by default the directory you cloned the repository at points at the master branch. This means that whenever you use 'git pull'
it updates the files contained to that directory with the files contained at the Master branch. In order to change the branch location of your local directory, hit:

`git checkout <branch name>`

For example, if you are located at a different branch and you want to change back into the master branch and drag its files, you have to execute 'git checkout master'. 
I'll explain the purpose of branches on the following section.

**#pushing files**

If you made any changes to your code and you want to push it back into your branch, **first of all, make sure your directory is pointing at your branch.**
Then, you have to execute:

`git add <filename>`

This sends the file into the "staging area". It is basically an extra security step. You can also use the dot character '.' to send every file from your directory 
to the staging area (e.g. 'git add .' If you want to remove a file from the staging area because you changed your mind, type this:

`git rm --cached <filename>`

You can also check which files are currently in the staging area by typing

`git status`

Which also shows any unstaged changes.

The next step after you have prepared your files at the staging area is to commit it. Again, make sure **you are located at your own branch.** One of the moderators will
soon merge your code to the master branch after making sure everything is good. We do want to avoid code conflicts at all costs, so please do not just add code to the master
branch. In order to push a file to your own branch, you have to type:

`git commit -m "lorem ipsum"`

Obviously, lorem ipsum is just a random message on this command example. The message you should really type there is a one-lined description of changes you did to your code since
the last commit, so its easier to keep track of changes log. Last but not least, to send the file. type:

`git push`

Note that if you want your code to be merged with the master branch, you should either send a merge request (simple enough via interface) or contact one of the moderators.

Contact info:

csd4333@csd.uoc.gr - George Manos



**#accessing previous commits**

Now, lets suppose you've made some changes to your code/repository, made plenty of commits, and for some reason you want to go back into an older version. Using git commands, 
you can pull older commits you or someone else made to the branch location you are at. To do so, first of all you have to check the changes log, using:

`git log`

You can also add '--oneline' parameter (and basically you better do so, in order to make the log easier to read). Without the oneline flag, the log format for each commit will
be like this:

`commit <id which is a long-ass random characters string>`

`Author: John Doe johndoe@email.com`

`Date: Mon May 4 20:35:54 2020 +0000`

Meanwhile, by adding the oneline flag, it should be like this:

`<short id> "lorem ipsum"`

Now, you can copy the short id and paste it to the following command so as to change into that specific commit.

`git checkout <short id>`

